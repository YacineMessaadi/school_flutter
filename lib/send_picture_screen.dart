import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:combien_g/analyse_details_screen.dart';
import 'package:http/http.dart' as http;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class SendPictureScreen extends StatelessWidget {
  // Chemin de la photo prise/importée depuis l'écran TakePictureScreen
  final String imagePath;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Récupération de l'adresse du serveur Flask depuis la variable d'environnement chargée par le fichier main
  final server = dotenv.env['server'];

  SendPictureScreen({Key? key, required this.imagePath}) : super(key: key);

  // Traitement de la réponse du serveur et envoi des informations à la page d'affichage de l'analyse
  void processResponse(response, context) async {
    response = await http.Response.fromStream(response);
    if (response.statusCode == 200) {
      const snackBar = SnackBar(content: Text('Image analysée avec succès !'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      await Navigator.of(context).push(
        MaterialPageRoute(
            builder: (context) =>
                Details(infos: jsonDecode(response.body)["prediction"])),
      );
    } else {
      const snackBar = SnackBar(content: Text("Erreur lors du traitement"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  // Envoi de l'image et de l'ID de l'utilisateur au serveur Flask, pour l'analyser, via une requête Multipart (texte et fichier)
  Future<void> upload(context) async {
    final request =
        http.MultipartRequest('POST', Uri.parse('$server/prediction'))
          ..fields.addAll({"userid": _auth.currentUser!.uid})
          ..headers.addAll({
            'Content-Type': 'multipart/form-data',
          })
          ..files.add(await http.MultipartFile.fromPath("img", imagePath));
    final response = await request.send();
    processResponse(response, context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      appBar: AppBar(
          backgroundColor: Colors.indigo, title: const Text('Votre photo :')),
      body: Image.file(File(imagePath)),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // Bouton de confirmation d'envoi
      floatingActionButton: FloatingActionButton(
        onPressed: () => {upload(context)},
        backgroundColor: Colors.indigo,
        foregroundColor: Colors.white,
        child: const Icon(Icons.upload_file_outlined),
      ),
    );
  }
}
