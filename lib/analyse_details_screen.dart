import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:http/http.dart' as http;

class Details extends StatefulWidget {
  const Details({Key? key, required this.infos}) : super(key: key);
  final dynamic infos;
  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseStorage storage = FirebaseStorage.instance;

  // Récupération de l'adresse du serveur Flask depuis la variable d'environnement chargée par le fichier main
  final server = dotenv.env['server'];

  // Variable de stockage des pièces telles que récupérées depuis le widget précédent
  List coins = [];
  // Variable de stockage de la correction faite par l'utilisateur
  List<Map<String, dynamic>> correction = [];

  @override
  void initState() {
    super.initState();
    coins = widget.infos;
    // Pré-remplissage de la correction avec les valeurs de l'analyse.
    for (var coin in coins) {
      correction.add({
        "id": coin[2],
        "value": (double.parse(coin[0]) * 100).toInt(),
        "side": "P",
        "confidence": coin[1]
      });
    }
  }

  // Création du menu déroulant pour corriger la valeur d'une pièce
  DropdownButton<int> buildValueDropdown(coin) {
    return DropdownButton<int>(
      value: coin["value"].toInt(),
      icon: const Icon(
        Icons.arrow_downward,
        color: Colors.black,
      ),
      elevation: 16,
      dropdownColor: Colors.white,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        color: Colors.black,
      ),
      onChanged: (int? newValue) {
        setState(() {
          coin["value"] = newValue;
        });
      },
      items: <int>[5, 10, 20, 50, 100, 200]
          .map<DropdownMenuItem<int>>((int value) {
        return DropdownMenuItem<int>(
          value: value,
          child: Text(
            (value.toDouble() / 100).toStringAsFixed(2) + "€",
            style: const TextStyle(fontSize: 18),
          ),
        );
      }).toList(),
    );
  }

  // Création du menu déroulant pour renseigner le côté d'une pièce
  DropdownButton<String> buildSideDropdown(coin) {
    return DropdownButton<String>(
      value: coin["side"],
      icon: const Icon(
        Icons.arrow_downward,
        color: Colors.black,
      ),
      elevation: 16,
      dropdownColor: Colors.white,
      style: const TextStyle(color: Colors.black),
      underline: Container(
        height: 2,
        color: Colors.black,
      ),
      onChanged: (String? newValue) {
        setState(() {
          coin["side"] = newValue;
        });
      },
      items: <String>["P", "F"].map<DropdownMenuItem<String>>((String value) {
        return DropdownMenuItem<String>(
          value: value,
          child: Text(
            value.characters.first == "P" ? "Pile" : "Face",
            style: const TextStyle(fontSize: 18),
          ),
        );
      }).toList(),
    );
  }

  // Récupération de l'URL de téléchargement de l'image d'une pièce
  Future<String> getImageUrl(coinId) async {
    var ref = storage.ref(auth.currentUser!.uid).child(coinId + '.jpg');
    return ref.getDownloadURL();
  }

  // Création de la liste des cards des pièces
  List<Widget> displayCoins() {
    List<Widget> rows = [];
    for (var coin in correction) {
      rows.add(FutureBuilder(
          future: getImageUrl(coin["id"]),
          builder: (BuildContext context, AsyncSnapshot<String> image) {
            if (image.hasData) {
              return Card(
                  margin: const EdgeInsets.symmetric(vertical: 20),
                  clipBehavior: Clip.antiAlias,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(24)),
                  elevation: 10,
                  color: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.network(
                        image.requireData,
                        height: 120,
                        fit: BoxFit.fill,
                      ),
                      Column(
                        children: [
                          const Text("Confiance",
                              style: TextStyle(color: Colors.black)),
                          Text(
                            double.parse(coin["confidence"].toString())
                                    .toStringAsFixed(2) +
                                "%",
                            style: const TextStyle(
                                color: Colors.black, fontSize: 16),
                          ),
                        ],
                      ),
                      Padding(
                          padding: const EdgeInsets.only(right: 15),
                          child: Column(
                            children: [
                              buildValueDropdown(coin),
                              buildSideDropdown(coin)
                            ],
                          ))
                    ],
                  ));
            } else {
              return const CircularProgressIndicator();
            }
          }));
    }
    rows.add(TextButton(
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(Colors.amber)),
        onPressed: () {
          sendCorrection();
        },
        child: const Text(
          "Corriger l'IA",
          style: TextStyle(color: Colors.black, fontSize: 20),
        )));
    return rows;
  }

  // Envoi de la correction au serveur Flask pour améliorer le dataset
  Future<dynamic> sendCorrection() {
    Map<String, dynamic> request = {
      "userid": auth.currentUser!.uid,
      "corrections": correction
    };
    return http
        .post(Uri.parse("$server/correction?userid=${auth.currentUser!.uid}"),
            headers: <String, String>{
              'Content-Type': 'application/json; charset=UTF-8',
            },
            body: json.encode(request))
        .then((response) {
      if (response.statusCode == 200) {
        const snackBar = SnackBar(content: Text("L'IA vous dit merci 😉"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      } else {
        const snackBar = SnackBar(content: Text("Erreur lors du traitement"));
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }).catchError((error) {
      const snackBar = SnackBar(
          content: Text("Erreur lors de l'envoi, vérifiez votre connexion"));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    });
  }

  // Calcul du montant total des pièces
  double total() {
    double sum = 0;
    for (var coin in coins) {
      sum += double.parse(coin[0]);
    }
    return sum;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Résultats de l'IA"),
          automaticallyImplyLeading: true,
        ),
        body: Container(
          alignment: Alignment.center,
          padding: const EdgeInsets.all(15),
          child: ListView(
            children: [
              Center(
                child: Text(
                  "Vous avez ${total()}€",
                  style: const TextStyle(fontSize: 25),
                ),
              ),
              const Center(
                  child: Text("Vous pouvez corriger les valeurs trouvées 🤓")),
              const Center(
                  child: Text(
                      "Cela permettra d'améliorer les futures prédictions")),
              ...displayCoins()
            ],
          ),
        ));
  }
}
