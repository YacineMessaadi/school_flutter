import 'dart:async';
import 'package:combien_g/login_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  // Initialisation de la connexion à Firebase
  await Firebase.initializeApp();
  // Chargement du fichier de variables d'environnement
  await dotenv.load(fileName: ".env");

  runApp(
    MaterialApp(
      theme: ThemeData.dark(),
      home: const SignInPage(),
    ),
  );
}
