import 'package:combien_g/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class AccountOptions extends StatefulWidget {
  const AccountOptions({Key? key}) : super(key: key);

  @override
  _AccountOptionsState createState() => _AccountOptionsState();
}

class _AccountOptionsState extends State<AccountOptions> {
  final FirebaseAuth auth = FirebaseAuth.instance;

  // Etat de stockage du mot de passe
  String password = "";

  // Réauthentification et suppression du compte de l'utilisateur
  Future _reauthenticateAndDelete(context) async {
    await auth.currentUser!.reauthenticateWithCredential(
        EmailAuthProvider.credential(
            email: auth.currentUser!.email!, password: password));
    await auth.currentUser!.delete();
    Navigator.of(context).pushAndRemoveUntil(
        MaterialPageRoute(builder: (context) => const SignInPage()),
        (Route<dynamic> route) => false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white30,
        body: Center(
          child: TextButton(
            child: const Text(
              "Supprimer le compte",
              style: TextStyle(color: Colors.white, fontSize: 16),
            ),
            style: TextButton.styleFrom(backgroundColor: Colors.red),
            onPressed: () async {
              // Je redemande à l'utilisateur son mot de passe car Firebase
              // exige que l'utilisateur se soit authentifié récemment pour pouvoir supprimer son compte
              showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      title: const Text('Entrez votre mot de passe'),
                      content: TextField(
                        obscureText: true,
                        onChanged: (value) {
                          setState(() {
                            password = value;
                          });
                        },
                        controller: TextEditingController(),
                        decoration:
                            const InputDecoration(hintText: "Mot de passe"),
                      ),
                      actions: <Widget>[
                        TextButton(
                            onPressed: () => _reauthenticateAndDelete(context),
                            child: const Text('Supprimer le compte')),
                      ],
                    );
                  });
            },
          ),
        ));
  }
}
