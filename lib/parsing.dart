import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';

// Création d'un FutureBuilder qui affichera l'image récupérée par la méthode extractOriginalImage
FutureBuilder<Image> futureImage(storage, userId, analyse) {
  return FutureBuilder(
    future: extractOriginalImage(storage, userId, analyse),
    builder: (BuildContext context, AsyncSnapshot<Image> image) {
      if (image.hasData) {
        return image.requireData;
      } else {
        return const CircularProgressIndicator();
      }
    },
  );
}

// Récupération d'une Image sur Firebase à partir de l'ID de fichier récupéré dans l'analyse
Future<Image> extractOriginalImage(storage, userId, analyse) async {
  Map<String, dynamic> analyseDetails = jsonDecode(analyse[3]);
  Map<String, dynamic> prediction = analyseDetails["prediction"];
  var ref = storage.ref(userId).child(prediction["original"] + '.jpg');

  return Image.network(
    await ref.getDownloadURL(),
    fit: BoxFit.cover,
    height: 240,
  );
}

// Récupération et formatage de la date de l'analyse
String getDate(analyse) {
  final date = DateTime.parse(analyse[0]);
  final format = DateFormat("dd/MM/yyyy à HH:mm:ss");
  return format.format(date);
}

// Calcul du montant total des pièces
double sum(analyse) {
  Map<String, dynamic> analyseDetails = jsonDecode(analyse[3]);
  Map<String, dynamic> prediction = analyseDetails["prediction"];
  double sum = 0;
  for (var coin in prediction["results"]) {
    sum += double.parse(coin[0]);
  }
  return sum;
}

// Récupération du tableau de résultats stocké dans l'analyse
List getCoins(analyse) {
  return analyse["prediction"]["results"];
}
