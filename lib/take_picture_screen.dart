// ignore_for_file: avoid_print

import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:image_picker/image_picker.dart';
import 'send_picture_screen.dart';

class TakePictureScreen extends StatefulWidget {
  const TakePictureScreen({
    Key? key,
    required this.camera,
  }) : super(key: key);

  final CameraDescription camera;

  @override
  TakePictureScreenState createState() => TakePictureScreenState();
}

class TakePictureScreenState extends State<TakePictureScreen> {
  // Création des états liés à l'utilisation de la caméra
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;
  bool _activatedFlash = false;

  // Par défaut, le flash est désactivé
  // J'utilise une haute résolution pour pouvoir avoir des découpages de pièces de bonne qualité côté serveur
  @override
  void initState() {
    super.initState();
    _controller = CameraController(
      widget.camera,
      ResolutionPreset.ultraHigh,
    );
    _controller.setFlashMode(FlashMode.off);
    _initializeControllerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      // J'utilise un FutureBuilder pour empêcher le crash de l'app en attendant l'ouverture de la caméra
      body: FutureBuilder<void>(
        future: _initializeControllerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // Affichage de la caméra sur toute la surface prévue
            final scale = 1 /
                (_controller.value.aspectRatio *
                    MediaQuery.of(context).size.aspectRatio);
            return Transform.scale(
              scale: scale,
              alignment: Alignment.topCenter,
              child: CameraPreview(_controller),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
      ),
      // Création des boutons flottants pour la prise de photo
      floatingActionButton: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 35),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              // Bouton permettant de sélectionner une image depuis la galerie du téléphone
              FloatingActionButton(
                heroTag: "explore",
                backgroundColor: Colors.white,
                foregroundColor: Colors.black,
                onPressed: () async {
                  final ImagePicker _picker = ImagePicker();
                  final XFile? image =
                      await _picker.pickImage(source: ImageSource.gallery);

                  if (image != null) {
                    await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => SendPictureScreen(
                          imagePath: image.path,
                        ),
                      ),
                    );
                  }
                },
                child: const Icon(Icons.image),
              ),
              // Bouton permettant de prendre une photo
              FloatingActionButton(
                heroTag: "capture",
                backgroundColor: Colors.amber,
                foregroundColor: Colors.indigo,
                onPressed: () async {
                  try {
                    await _initializeControllerFuture;
                    final image = await _controller.takePicture();

                    await Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => SendPictureScreen(
                          imagePath: image.path,
                        ),
                      ),
                    );
                  } catch (e) {
                    print("e");
                  }
                },
                child: const Icon(Icons.camera_alt),
              ),
              // Bouton permettant d'activer/désactiver le flash
              // Ne cause pas de bug si l'appareil n'a pas de flash (testé sur tablette)
              FloatingActionButton(
                heroTag: "flash",
                backgroundColor: _activatedFlash ? Colors.white : Colors.black,
                foregroundColor: _activatedFlash ? Colors.black : Colors.white,
                onPressed: () {
                  setState(() {
                    _activatedFlash = !_activatedFlash;
                  });
                  if (_activatedFlash) {
                    _controller.setFlashMode(FlashMode.always);
                  } else {
                    _controller.setFlashMode(FlashMode.off);
                  }
                },
                child: const Icon(Icons.bolt),
              ),
            ],
          )),
    );
  }
}
