import 'dart:convert';

import 'package:combien_g/parsing.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:combien_g/analyse_details_screen.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class BrowsePictures extends StatelessWidget {
  BrowsePictures({Key? key}) : super(key: key);
  final FirebaseAuth auth = FirebaseAuth.instance;
  final FirebaseStorage storage = FirebaseStorage.instance;

  // Récupération de l'adresse du serveur Flask depuis la variable d'environnement chargée par le fichier main
  final server = dotenv.env['server'];

  // Récupération des analyses de l'utilisateur depuis le serveur Flask
  Future<dynamic> getAnalyses() {
    return http
        .get(Uri.parse("$server/history?userid=${auth.currentUser!.uid}"))
        .then((response) {
      return jsonDecode(response.body);
    });
  }

  // Création d'une Card pour afficher une analyse
  // Affiche la photo prise, la date, le montant détecté et un bouton pour accéder aux détails
  Widget buildCard(infos, image, context) {
    return Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        elevation: 10,
        color: Colors.white,
        child: Column(children: [
          Padding(
            child: image,
            padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
          ),
          Padding(
            padding: const EdgeInsets.all(16).copyWith(bottom: 0),
            child: Text(
              "Analyse du " + getDate(infos),
              style: const TextStyle(color: Colors.black),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  sum(infos).toString() + "€",
                  style: const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 24),
                ),
                TextButton(
                    onPressed: () async {
                      List analyseDetails =
                          jsonDecode(infos[3])["prediction"]["results"];
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) =>
                                Details(infos: analyseDetails)),
                      );
                    },
                    child: const Text("Détails"))
              ],
            ),
          )
        ]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white30,
        body: Container(
            padding: const EdgeInsets.all(5.0),
            child: FutureBuilder<dynamic>(
                future: getAnalyses(),
                builder: (context, AsyncSnapshot<dynamic> snapshot) {
                  if (snapshot.hasData) {
                    final analyses = snapshot.data["analyses"];
                    if (analyses.isEmpty) {
                      return const Center(child: Text('Aucune analyse...'));
                    }
                    return ListView.builder(
                        padding: const EdgeInsets.all(16),
                        itemCount: analyses.length,
                        itemBuilder: (BuildContext context, int index) {
                          var doc = analyses[index];
                          return buildCard(
                              doc,
                              futureImage(storage, auth.currentUser!.uid, doc),
                              context);
                        });
                  }
                  return const CircularProgressIndicator();
                })));
  }
}
