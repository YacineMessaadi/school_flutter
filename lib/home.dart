import 'package:combien_g/account_options_screen.dart';
import 'package:combien_g/browse_pictures_screen.dart';
import 'package:combien_g/take_picture_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';

class Home extends StatefulWidget {
  const Home({Key? key, required this.camera}) : super(key: key);

  final CameraDescription camera;
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  // Création des états qui serviront à passer d'un écran à l'autre via la BottomNavBar
  int _index = 0;
  List<Widget> _pages = [];

  @override
  void initState() {
    _pages = [
      TakePictureScreen(camera: widget.camera),
      BrowsePictures(),
      const AccountOptions(),
    ];

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  // Changement de page lorsqu'une icône est sélectionnée
  void _onItemTapped(int index) {
    setState(() {
      _index = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    // J'utilise WillPopScope pour empêcher le retour à l'écran de connexion via un geste pris en charge par les OS mobiles (swipe de gauche à droite qui enclenche un retour arrière)
    return WillPopScope(
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
              actions: [
                TextButton(
                    // Déconnexion si l'on appuie sur l'icône de l'AppBar
                    onPressed: () async {
                      await _auth.signOut();
                      Navigator.popUntil(context, ModalRoute.withName("/"));
                    },
                    child: const Icon(
                      Icons.logout,
                      color: Colors.white,
                    ))
              ],
              automaticallyImplyLeading: false,
              backgroundColor: Colors.black,
              title: const Text('Combien G')),
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.camera),
                label: "Let's go",
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.folder),
                label: 'Archives',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.person),
                label: 'Compte',
              ),
            ],
            currentIndex: _index,
            selectedItemColor: Colors.white,
            onTap: _onItemTapped,
          ),
          body: _pages[_index],
        ),
        onWillPop: () async => false);
  }
}
