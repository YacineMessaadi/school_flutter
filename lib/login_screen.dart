import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:combien_g/home.dart';
import 'package:firebase_auth/firebase_auth.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

// Récupération de la caméra en amont de la redirection vers la page principal de l'app
void goToHome(BuildContext context) async {
  final List<CameraDescription> cameras = await availableCameras();
  Navigator.push(context,
      MaterialPageRoute(builder: (context) => Home(camera: cameras.first)));
}

// Création du message d'erreur de connexion
class ScaffoldSnackbar {
  ScaffoldSnackbar(this._context);
  final BuildContext _context;

  factory ScaffoldSnackbar.of(BuildContext context) {
    return ScaffoldSnackbar(context);
  }

  void show(String message) {
    ScaffoldMessenger.of(_context)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(content: Text(message)),
      );
  }
}

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key, dynamic camera}) : super(key: key);

  final String title = 'Combien G';

  @override
  State<StatefulWidget> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  User? user;

  @override
  void initState() {
    // Si l'utilisateur est déjà connecté, on passe directement au coeur de l'app
    if (_auth.currentUser != null) {
      goToHome(context);
    }

    // Création d'un Observer pour changer l'état user en cas de changement du user Firebase
    _auth.userChanges().listen(
          (event) => setState(() => user = event),
        );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(widget.title),
      ),
      body: Builder(
        builder: (BuildContext context) {
          return ListView(
            padding: const EdgeInsets.all(8),
            children: const <Widget>[
              _EmailPasswordForm(),
            ],
          );
        },
      ),
    );
  }
}

// Création de la classe dont les états sont le mot de passe et l'email, afin de les récupérer selon les entrées du formulaire
class _EmailPasswordForm extends StatefulWidget {
  const _EmailPasswordForm({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _EmailPasswordFormState();
}

class _EmailPasswordFormState extends State<_EmailPasswordForm> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      const Padding(
        padding: EdgeInsets.all(30),
        child: Text(
          "Bienvenue !",
          style: TextStyle(color: Colors.black, fontSize: 30),
        ),
      ),
      Form(
        key: _formKey,
        child: Card(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
          color: Colors.black,
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: const Text(
                    'Connectez-vous !',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
                TextFormField(
                  controller: _emailController,
                  decoration: const InputDecoration(labelText: 'Email'),
                  validator: (String? value) {
                    if (value!.isEmpty) return 'Entrez votre email...';
                    return null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  decoration: const InputDecoration(labelText: 'Mot de passe'),
                  validator: (String? value) {
                    if (value!.isEmpty) return 'Entrez votre mot de passe';
                    return null;
                  },
                  obscureText: true,
                ),
                Container(
                    padding: const EdgeInsets.only(top: 16),
                    alignment: Alignment.center,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        TextButton(
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.blue[900],
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20)),
                          child: const Text(
                            'Inscription',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              await _signUpWithEmailAndPassword();
                            }
                          },
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                              backgroundColor: Colors.blueGrey,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20)),
                          child: const Text(
                            'Connexion',
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () async {
                            if (_formKey.currentState!.validate()) {
                              await _signInWithEmailAndPassword();
                            }
                          },
                        ),
                      ],
                    )),
              ],
            ),
          ),
        ),
      ),
    ]);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  // Authentification à Firebase avec les informations récupérées des états
  Future<void> _signInWithEmailAndPassword() async {
    try {
      final User user = (await _auth.signInWithEmailAndPassword(
        email: _emailController.text,
        password: _passwordController.text,
      ))
          .user!;
      ScaffoldSnackbar.of(context).show('${user.email} connecté !');
      goToHome(context);
    } catch (e) {
      ScaffoldSnackbar.of(context).show('Échec de la connexion');
    }
  }

  // Inscription et authentification directe à Firebase avec les informations récupérées des états
  Future<void> _signUpWithEmailAndPassword() async {
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: _emailController.text,
        password: _passwordController.text,
      );
      ScaffoldSnackbar.of(context)
          .show('${userCredential.user!.email} connecté !');
      goToHome(context);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'weak-password') {
        ScaffoldSnackbar.of(context).show('Mot de passe trop faible');
      } else if (e.code == 'email-already-in-use') {
        ScaffoldSnackbar.of(context).show('Adresse email déjà utilisée');
      }
    } catch (e) {
      ScaffoldSnackbar.of(context).show(e.toString());
    }
  }
}
